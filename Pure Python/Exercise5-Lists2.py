#Exercise 5: Using python list operations to manage some pizza data
toppins = ['pepperoni', 'pineaple', 'cheese', 'sausage', 'olives', 'anchovies', 'mushrooms']
prices = [2, 6, 1, 3, 2, 7, 2]

num_pizzas  = len(toppins)

print("We sell",str(num_pizzas),"different kinds of pizza!")

pizzas = list(zip( prices, toppins))
pizzas.sort()

print(pizzas)

cheapest_pizza = pizzas[0]
priciest_pizza  =pizzas[-1]
three_cheapest = pizzas[:3]

print(three_cheapest)

num_two_dollar_slices = prices.count(2)