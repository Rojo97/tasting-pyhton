#Exercise 3: Using python flow control to make a shipping cost comparator
premium_ground_shipping_cost = 125.0

def ground_shipping_cost(wheight):
  flat_charge = 20.0
  if wheight <= 2:
    return 1.5*wheight +flat_charge
  elif wheight > 2 and wheight <= 6:
    return 3.0*wheight +flat_charge
  elif wheight > 6 and wheight <= 10:
    return 4.0*wheight +flat_charge
  else:
    return 4.75*wheight +flat_charge
  
def drone_shipping_cost(wheight):
  if wheight <= 2:
    return 4.5*wheight 
  elif wheight > 2 and wheight <= 6:
    return 9.0*wheight 
  elif wheight > 6 and wheight <= 10:
    return 12.0*wheight 
  else:
    return 14.25*wheight
  
def best_shipping(wheight):
  drone = drone_shipping_cost(wheight)
  ground = ground_shipping_cost(wheight)
  if(drone < ground and drone < premium_ground_shipping_cost):
    return "Drone shipping is cheaper, only cost: " + str(drone)
  elif(ground < drone and ground < premium_ground_shipping_cost):
    return "Ground shipping is cheaper, only cost: " + str(ground)
  else:
    return "Premiun ground shipping is cheaper, only cost: " + str(premium_ground_shipping_cost)
  
print(best_shipping(41.5))